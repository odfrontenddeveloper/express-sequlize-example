const { Sequelize, DataTypes } = require("sequelize");
const sequelize = require("../database");
const Author = require("./author");

const Book = sequelize.define("books", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
  },
  author_id: {
    type: DataTypes.INTEGER,
  },
  name: {
    type: DataTypes.STRING,
  },
});

module.exports = Book;
