const { Sequelize, DataTypes } = require("sequelize");
const sequelize = require("../database");
const Book = require("./book");

const Author = sequelize.define("authors", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING,
  },
});

module.exports = Author;
