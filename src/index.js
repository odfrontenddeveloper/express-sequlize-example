require("dotenv").config();
require("./database");
require("./models");
const express = require("express");
const cors = require("cors");
var bodyParser = require("body-parser");
const urlEncoderParser = bodyParser.urlencoded({ extended: false });
const compression = require("compression");

const author = require("./router/author");
const book = require("./router/book");

const app = express();

app.use(compression());
app.use(cors());
app.use(express.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(urlEncoderParser);

app.use("/author", author);
app.use("/book", book);

app.listen(+process.env.PORT, (err) => {
  err
    ? console.log(err)
    : console.log(`Server started on port ${process.env.PORT} ⚡️`);
});
