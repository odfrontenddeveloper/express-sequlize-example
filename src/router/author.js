const Router = require("express").Router;
const islogin = require("../middleware/index").islogin;
const Author = require("../models/author");

const router = Router();

router.get("/", islogin, async (req, res) => {
  const authors = await Author.findAll({
    attributes: ["id", "name"],
  });
  res.send(authors);
});

router.get("/:id", islogin, async (req, res) => {
  const author = await Author.findOne({
    where: {
      id: req.params.id,
    },
    attributes: ["id", "name"],
  });
  res.send(author);
});

router.post("/", islogin, async (req, res) => {
  const { author } = req.body;
  const newauthor = await Author.create({ name: author });
  res.send(newauthor);
});

router.put("/", islogin, async (req, res) => {
  const { id, author } = req.body;
  const newauthor = await Author.update(
    {
      name: author,
    },
    {
      where: {
        id: id,
      },
    }
  );
  res.send(newauthor);
});

router.delete("/:id", islogin, async (req, res) => {
  const { id } = req.params;
  const deleteauthor = await Author.destroy({
    where: {
      id: id,
    },
  });
  res.send(id);
});

module.exports = router;
