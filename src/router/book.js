const Router = require("express").Router;
const islogin = require("../middleware/index").islogin;
const Book = require("../models/book");

const router = Router();

router.get("/", islogin, async (req, res) => {
  const books = await Book.findAll({
    attributes: ["id", "author_id", "name"],
  });
  res.send(books);
});

router.get("/byauthor/:id", islogin, async (req, res) => {
  const books = await Book.findAll({
    attributes: ["id", "author_id", "name"],
    where: {
      author_id: req.params.id,
    },
  });
  res.send(books);
});

module.exports = router;
